import React from 'react';
import Nav from './Nav';
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm';
import PresentationForm from './PresentationForm';
import { BrowserRouter, Route, Routes} from 'react-router-dom'
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />
    <div className="container">
      <Routes>
        <Route path="locations/new" element={<LocationForm />} />
        <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="attendees/new" element={<AttendeeForm />} />
        <Route path="conferences/new" element={<ConferenceForm />} />
        <Route path="presentations/new" element={<PresentationForm />} />
        <Route index path="/" element={<MainPage />} />
      </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
